<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/articles', 'ArticleController@index')->name('articles');
Route::middleware('auth:api')->group(function () {
    Route::post('/articles/create', 'ArticleController@store')->name('articles.create');
    Route::delete('/articles/{id}', 'ArticleController@destroy')->name('articles.delete');
    Route::patch('/articles/{id}', 'ArticleController@update')->name('articles.delete');
});
Route::group(['middleware' => []], function () {

    // ...

    // public routes
    Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register','Auth\ApiAuthController@register')->name('register.api');
    Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');


    // ...

});
